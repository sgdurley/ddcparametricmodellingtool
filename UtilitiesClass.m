%{

UtilitiesClass.m

Title:
Utitlities Class used by
Developable Double-Corrugated (DDC) Surface Shell Element Parametric Modelling Tool

Author:
Samuel Durley

Date:
April 2019

%}

classdef UtilitiesClass
    properties (Constant)

        debug = false;
        
        validSides = {'t', 'r', 'b', 'l'};
        validDirections = {'x','y','z'};
        validCsvAll = 'all';
    end
    methods (Static)
        
        function doLog(value)
            % only log if debug set to true
            if UtilitiesClass.debug
                disp(['LOG: ', value]);
            end
        end
         
        function [dt] = getFileName()
            % return the filename in the form datetime.inp
            dt = datestr(now,'yyyymmddHHMMSSFFF.inp');
        end

        function TF = isNumber(value)
            TF = false;
            UtilitiesClass.doLog(value);
            valueNum = str2double(value);
            % if invalid - str2double will return NaN (Not a Number)
            if isnan(valueNum)
                UtilitiesClass.doLog('isnan');
            else
                UtilitiesClass.doLog('is number');
                TF = true;
            end
        end

        function TF = isInteger(value)
            TF = false;
            if UtilitiesClass.isNumber(value)
                % str2double - also done in isNumber - but repeated here as needed
                % for floor function and comparison
                valueNum = str2double(value);
                % floor rounds to the nearest integer less than or equal to value
                floorNum = floor(valueNum);
                % check value as a number and floor value are the same - if so - we
                % have an integer
                if valueNum == floorNum
                    UtilitiesClass.doLog('is integer');
                    TF = true;
                else
                    UtilitiesClass.doLog('number NOT equal to floor');
                end
            end
        end

        function TF = isPositive(value)
            TF = false;
            % str2double - needed for comparison
            valueNum = str2double(value);
            if valueNum > 0
                UtilitiesClass.doLog('is positive');
                TF = true;
            else
                UtilitiesClass.doLog('is NOT positive');
            end
        end

        function TF = isEven(value)
            TF = false;
            % str2double - needed for modulus
            valueNum = str2double(value);
            if mod(valueNum, 2) == 0
                UtilitiesClass.doLog('is even');
                TF = true;
            else
                UtilitiesClass.doLog('is NOT even');
            end
        end

        function TF = isOdd(value)
            TF = false;
            % check if not even :)
            if ~UtilitiesClass.isEven(value)
                TF = true;
            end
        end
        
        function TF = isBetween(value, from, to)
            TF = false;
            % str2double - needed for comparison
            valueNum = str2double(value);
            if valueNum >= from && valueNum <= to
                UtilitiesClass.doLog('is between');
                TF = true;
            else
                UtilitiesClass.doLog('is NOT between');
            end
        end

        function TF = isGreaterThan(value, greaterThan)
            TF = false;
            % str2double - needed for comparison
            valueNum = str2double(value);
            if valueNum > greaterThan
                UtilitiesClass.doLog('is greater than');
                TF = true;
            else
                UtilitiesClass.doLog('is NOT greater than');
            end
        end

        function TF = isGreaterThanOrEqual(value, greaterThanOrEqual)
            TF = false;
            % str2double - needed for comparison
            valueNum = str2double(value);
            if valueNum >= greaterThanOrEqual
                UtilitiesClass.doLog('is greater than or equal');
                TF = true;
            else
                UtilitiesClass.doLog('is NOT greater than or equal');
            end
        end

        function TF = isPositiveNumber(value)
            TF = false;
            if UtilitiesClass.isNumber(value) && UtilitiesClass.isPositive(value)
                UtilitiesClass.doLog('is positive number');
                TF = true;
            end
        end

        function TF = isPositiveInteger(value)
            TF = false;
            if UtilitiesClass.isInteger(value) && UtilitiesClass.isPositive(value)
                UtilitiesClass.doLog('is positive integer');
                TF = true;
            end
        end

        function TF = isNumberBetween(value, from, to)
            TF = false;
            if UtilitiesClass.isNumber(value) && UtilitiesClass.isBetween(value, from, to)
                UtilitiesClass.doLog('is number between');
                TF = true;
            end
        end

        function TF = isIntegerBetween(value, from, to)
            TF = false;
            if UtilitiesClass.isInteger(value) && UtilitiesClass.isBetween(value, from, to)
                UtilitiesClass.doLog('is integer between');
                TF = true;
            end
        end

        function TF = isNumberGreaterThan(value, greaterThan)
            TF = false;
            if UtilitiesClass.isNumber(value) && UtilitiesClass.isGreaterThan(value, greaterThan)
                UtilitiesClass.doLog('is number greater than');
                TF = true;
            end
        end

        function TF = isIntegerGreaterThan(value, greaterThan)
            TF = false;
            if UtilitiesClass.isInteger(value) && UtilitiesClass.isGreaterThan(value, greaterThan)
                UtilitiesClass.doLog('is integer greater than');
                TF = true;
            end
        end
        
        function TF = isNumberGreaterThanOrEqual(value, greaterThanOrEqual)
            TF = false;
            if UtilitiesClass.isNumber(value) && UtilitiesClass.isGreaterThanOrEqual(value, greaterThanOrEqual)
                UtilitiesClass.doLog('is number greater than or equal');
                TF = true;
            end
        end

        function TF = isIntegerGreaterThanOrEqual(value, greaterThanOrEqual)
            TF = false;
            if UtilitiesClass.isInteger(value) && UtilitiesClass.isGreaterThanOrEqual(value, greaterThanOrEqual)
                UtilitiesClass.doLog('is integer greater than or equal');
                TF = true;
            end
        end
        
        function TF = isOddPositiveInteger(value)
            TF = false;
            if UtilitiesClass.isPositiveInteger(value) && UtilitiesClass.isOdd(value)
                UtilitiesClass.doLog('is odd positive integer');
                TF = true;
            end
        end
        
        function TF = isValidMember(valids, value)
            TF = false;
            UtilitiesClass.doLog(value);
            % check if value is a member of valids
            if ismember(value, valids)
                UtilitiesClass.doLog('is valid member');
                TF = true;
            end
        end

        function TF = isValidCsvBetween(csv, from, to)
            TF = false;
            UtilitiesClass.doLog(csv);

            % split the comma separated input into an array so we can validate each
            csvArray = split(csv, ",");

            % check no duplicates exists by using unique function
            uniqueCsvArray = unique(csvArray);
            if size(uniqueCsvArray) == size(csvArray)
                % no duplicates so continue ...
                allValid = true;
                for k=1:size(csvArray)
                    % loop through and validate each
                    item = csvArray(k);
                    % trim the item first to remove unwanted spaces and update the array 
                    trimmed = strtrim(item);
                    % check item is between from and to
                    if UtilitiesClass.isIntegerBetween(trimmed, from, to)
                        UtilitiesClass.doLog('valid');
                        %fprintf('item %s is valid - %s\n',[k, trimmed]);
                    else
                        UtilitiesClass.doLog('invalid');
                        %fprintf('item %s is INVALID - %s\n',[k, trimmed]);
                        allValid = false;
                    end
                end

                if allValid
                    TF = true;
                end
            else
                UtilitiesClass.doLog('duplicates exist');
            end
        end

        function translated = translateFromTo(value, from, to) 

            UtilitiesClass.doLog(['translate - ', value]);
            
            % value = selected value - that 'should' exist in from array
            % from = array of values we want to translate from
            % to = array of values we want to translate to

            % find the index of the selected (value) in the from array
            index = strcmp(from, value);
            
            % could be checking from and to array lengths are the same
            % should be checking that value has been found
            
            % return the value from the to (translated) array
            translated = to(index);

            UtilitiesClass.doLog(['translated - ', translated]);

        end
                
    end
end
