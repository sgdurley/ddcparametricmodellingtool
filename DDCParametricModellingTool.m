%{

DDCParametricModellingTool.m

Title:
Developable Double-Corrugated (DDC) Surface Shell Element Parametric Modelling Tool

Author:
Samuel Durley

Date:
April 2019

%}

clc;
clear;

% ***** take user inputs for MATLAB model *****

valid = 0;
prompt = 'What is straight side length a? (number > 0) ';
while ~valid
  a = input(prompt, 's');
  if UtilitiesClass.isNumberGreaterThan(a, 0)
    % convert from string
    a = str2double(a);
    valid = 1;
  else
    disp('>>> Invalid <<<');
  end
end

valid = 0;
prompt = 'What is straight side length b? (number > 0) ';
while ~valid
  b = input(prompt, 's');
  if UtilitiesClass.isNumberGreaterThan(b, 0)
    % convert from string
    b = str2double(b);
    valid = 1;
  else
    disp('>>> Invalid <<<');
  end
end

valid = 0;
betweenFrom = 0;
betweenTo = 90;
prompt = ['What is internal angle gamma? (number between ', num2str(betweenFrom), ' and ', num2str(betweenTo), ') '];
while ~valid
  gammadeg = input(prompt, 's');
  if UtilitiesClass.isNumberBetween(gammadeg, betweenFrom, betweenTo)
    % convert from string
    gammadeg = str2double(gammadeg);
    valid = 1;
  else
    disp('>>> Invalid <<<');
  end
end

valid = 0;
prompt = 'How many faces in x direction? (integer > 1) ';
while ~valid
  nofacx = input(prompt, 's');
  if UtilitiesClass.isIntegerGreaterThan(nofacx, 1)
    % convert from string
    nofacx = str2double(nofacx);
    valid = 1;
  else
    disp('>>> Invalid <<<');
  end
end

valid = 0;
prompt = 'How many faces in y direction? (integer > 1) ';
while ~valid 
  nofacy = input(prompt, 's');
  if UtilitiesClass.isIntegerGreaterThan(nofacy, 1)
    % convert from string
    nofacy = str2double(nofacy);
    valid = 1;
  else
    disp('>>> Invalid <<<');
  end
end

valid = 0;
betweenFrom = 0;
betweenTo = 90;
prompt = ['What is the selected dihedral development angle? (number between ', num2str(betweenFrom), ' and ', num2str(betweenTo), ') '];
while ~valid
  thetadeg = input(prompt, 's');
  if UtilitiesClass.isNumberBetween(thetadeg, betweenFrom, betweenTo)
    % convert from string
    thetadeg = str2double(thetadeg);
    valid = 1;
  else
    disp('>>> Invalid <<<');
  end
end

% ***** use inputs *****

% conversion from degrees to radians
gamma=gammadeg*pi/180;
theta=thetadeg*pi/180;

% calculate number of elements
NE=nofacx*nofacy;
% number of crease lines in each direction
m=nofacx+1;
n=nofacy+1;
% calculate the number of nodes
N=m*n;

% general equations for unit cell height, width, length and amplitude
H=a*sin(theta)*sin(gamma);
S=b*(cos(theta)*tan(gamma))/sqrt(1+cos(theta).^2*tan(gamma).^2);
L=a*sqrt(1-sin(theta).^2*sin(gamma).^2);
V=b*(1/sqrt(1+(cos(theta).^2)*(tan(gamma).^2)));

% create empty coordinate matrix for nodes
T=zeros(N,3);

% find number in list given i and j t=(j-1)*m+i
% wipe graph
hold off

% calc x
for k=1:n
    for r=1:m
      T((k-1)*m+r,1)=(r-1)*S;
    end
end

% calc y for odd i
for k=1:n
    for r=1:2:m
        T((k-1)*m+r,2)=(k-1)*L;
    end
end

% calc y for even i
for k=1:n
    for r=2:2:m
        T((k-1)*m+r,2)=(k-1)*L+V;
    end
end

% calc z for even j
for k=2:2:n
    for r=1:m
       T((k-1)*m+r,3)=H; 
    end
end


% preliminary nodal plot
X=T(1:N,1);
Y=T(1:N,2);
Z=T(1:N,3);

scatter3(X,Y,Z)
xlabel('x')
ylabel('y')
zlabel('z')

% add labels to nodes 
a = (1:N)'; b = num2str(a); c = cellstr(b);
dx = 0.1; 
dy = 0.1;
dz = 0.1;
% displacement so the text does not overlay the data points
text(X+dx, Y+dy, Z+dz, c);

% retain plot values
hold all

% element node assignment
E=zeros(NE,5);

for k=1:NE
    % element number
    E(k,1)=k;
    % assign rows
    r=k/nofacx;
    row=ceil(r);
    E(k,2)=k+row-1;
    E(k,3)=k+row;
    E(k,4)=k+row+m;
    E(k,5)=k+row-1+m;
end

hold all

% line plot connecting nodes
for k=1:NE
    plot3([T(E(k,2),1),T(E(k,3),1)],[T(E(k,2),2),T(E(k,3),2)],[T(E(k,2),3),T(E(k,3),3)])
    plot3([T(E(k,3),1),T(E(k,4),1)],[T(E(k,3),2),T(E(k,4),2)],[T(E(k,3),3),T(E(k,4),3)])
    plot3([T(E(k,4),1),T(E(k,5),1)],[T(E(k,4),2),T(E(k,5),2)],[T(E(k,4),3),T(E(k,5),3)])
    plot3([T(E(k,5),1),T(E(k,2),1)],[T(E(k,5),2),T(E(k,2),2)],[T(E(k,5),3),T(E(k,2),3)])
    axis equal
end

%{
--------------------------------------------------------------
----- all values needed for model DONE give user options -----
--------------------------------------------------------------
%}

disp(' ');
disp('Your geometric model has now been created in MATLAB - see figure viewer');
disp(' ');

valid = 0;
please1 = 'Please select an option from the following choices\n ';
opt1 = '1. End the program\n ';
opt2 = '2. Create an ABAQUS input file for the current figure\n ';
opt3 = '3. Continue developing input file to include material properties, boundary conditions and loading conditions\n';
please2 = 'Please enter 1, 2, or 3\n';
prompt = strcat(please1, opt1, opt2, opt3, please2);
while ~valid 
  whatNext = input(prompt, 's');
  if UtilitiesClass.isValidMember({'1', '2', '3'}, whatNext)
    % convert from string
    whatNext = str2double(whatNext);
    valid = 1;
  else
    disp('>>> Invalid <<<');
  end
end

% default boundaries value
% - needed in case when user decides not to continue but still wants .inp
% file to be written
boundaries = '';

if whatNext > 2 % user has opted to enter additional information ...

%{
    doContinue start ...
    doContinue start ...
    doContinue start ...
%}
            
    disp('MATERIAL PROPERTIES:');

    % number greater than 0
    valid = 0;
    prompt = 'What is the modulus of elasticity? (number greater than 0) ';
    while ~valid 
      Youngs = input(prompt, 's');
      if UtilitiesClass.isNumberGreaterThan(Youngs, 0)
        % convert from string
        Youngs = str2double(Youngs);
        valid = 1;
      else
        disp('>>> Invalid <<<');
      end
    end

    % number between -1 and 0.5
    valid = 0;
    prompt = 'What is Poisson''s ratio? (number between -1 and 0.5) ';
    while ~valid 
      Poisson = input(prompt, 's');
      if UtilitiesClass.isNumberBetween(Poisson, -1, 0.5)
        % convert from string
        Poisson = str2double(Poisson);
        valid = 1;
      else
        disp('>>> Invalid <<<');
      end
    end

    % number greater than 0
    valid = 0;
    prompt = 'What is the shell thickness? (number greater than 0) ';
    while ~valid 
      thickness = input(prompt, 's');
      if UtilitiesClass.isNumberGreaterThan(thickness, 0)
        % convert from string
        thickness = str2double(thickness);
        valid = 1;
      else
        disp('>>> Invalid <<<');
      end
    end

    % odd integer greater than 0 
    valid = 0;
    prompt = 'What is the number of integration points through the thickness? (odd integer greater than 0) ';
    while ~valid 
      intPoints = input(prompt, 's');
      if UtilitiesClass.isOddPositiveInteger(intPoints)
        % convert from string
        intPoints = str2double(intPoints);
        valid = 1;
      else
        disp('>>> Invalid <<<');
      end
    end

    valid = 0;
    prompt = 'Do you want to apply boundary conditions? (y/n) ';
    while ~valid 
      boundaryConditions = input(prompt, 's');
      if UtilitiesClass.isValidMember({'y', 'n'}, boundaryConditions)
        valid = 1;
      else
        disp('>>> Invalid <<<');
      end
    end

    if strcmp(boundaryConditions, 'y')

        disp('BOUNDARY CONDITIONS:');

        valid = 0;
        prompt = ['Which nodes are to be fixed?\n'...
            '(sides t=top, r=right, b=bottom, l=left '...
            'or integers separated by commas between 1 and ', num2str(N), ') '];
        while ~valid
            boundaries = input(prompt, 's');
            if UtilitiesClass.isValidMember(UtilitiesClass.validSides, boundaries)
                % user has selected to fix a single side t, r, b, l
                valid = 1;
            else
                if UtilitiesClass.isValidCsvBetween(boundaries, 1, N)
                    % user has entered a valid comma separated node list
                    valid = 1;
                else
                    disp('>>> Invalid <<<');
                end
            end
        end
        %disp(boundaries);
    end

    disp('LOADING CONDITIONS:');

    valid = 0;
    prompt = 'How many concentrated loads of different magnitude/direction are there? (integer greater than or equal to 0) ';
    while ~valid
      numCLoad = input(prompt, 's');
      if UtilitiesClass.isIntegerGreaterThanOrEqual(numCLoad, 0)
        % convert from string
        numCLoad = str2double(numCLoad);
        valid = 1;
      else
        disp('>>> Invalid <<<');
      end
    end

    CLoadNodes=strings(numCLoad,1);
    CLoadMag=zeros(numCLoad,1);
    CLoadDir=strings(numCLoad,1);

    for k=1:numCLoad
        
        % loop through the number of concentrated load and ask ...
        % - which nodes
        % - which direction
        % - what magnitude
        
        valid = 0;
        prompt = ['Which nodes does concentrated load ', num2str(k), ' act upon? (all or integers separated by commas between 1 and ', num2str(N), ') '];
        while ~valid
          CLoadNodes(k,1) = input(prompt, 's');
          if strcmp(CLoadNodes(k,1),UtilitiesClass.validCsvAll)
              % all is valid and will be handled as everything
              valid = 1;
          else
              if UtilitiesClass.isValidCsvBetween(CLoadNodes(k,1), 1, N)
                valid = 1;
              else
                disp('>>> Invalid <<<');
              end
          end    
        end

        valid = 0;
        prompt = 'In what direction does it act? (x, y, z) ';
        while ~valid
          direction = input(prompt, 's');
          if UtilitiesClass.isValidMember(UtilitiesClass.validDirections, direction)
            valid = 1;
            % translateDirection from value entered (xyz) to ...
            CLoadDir(k,1) = UtilitiesClass.translateFromTo(direction, UtilitiesClass.validDirections, {'1', '2', '3'});
          else
            disp('>>> Invalid <<<');
          end
        end

        valid = 0;
        prompt = 'What is its magnitude? (number) ';
        while ~valid
          magnitude = input(prompt, 's');
          if UtilitiesClass.isNumber(magnitude)
            % convert from string
            CLoadMag(k,1) = str2double(magnitude);
            valid = 1;
          else
            disp('>>> Invalid <<<');
          end
        end

    end
    
    valid = 0;
    prompt = 'How many distributed loads of different magnitude/direction are there? (integer greater than or equal to 0) ';
    while ~valid
      numDLoad = input(prompt, 's');
      if UtilitiesClass.isIntegerGreaterThanOrEqual(numDLoad, 0)
        % convert from string
        numDLoad = str2double(numDLoad);
        valid = 1;
      else
        disp('>>> Invalid <<<');
      end
    end

    DLoadNodes=strings(numDLoad,1);
    DLoadMag=zeros(numDLoad,1);
    DLoadDir=strings(numDLoad,1);

    for k=1:numDLoad

        % loop through the number of distrubuted load and ask ...
        % - which elements
        % - which direction
        % - what magnitude

        valid = 0;
        prompt = ['Which elements does distrubuted load ', num2str(k), ' act upon? (all or integers separated by commas) (1-', num2str(NE), ') '];
        while ~valid
          DLoadNodes(k,1) = input(prompt, 's');
          if strcmp(DLoadNodes(k,1),UtilitiesClass.validCsvAll)
              % all is valid and will be handled as everything
              valid = 1;
          else
              if UtilitiesClass.isValidCsvBetween(DLoadNodes(k,1), 1, N)
                valid = 1;
              else
                disp('>>> Invalid <<<');
              end
          end    
        end

        valid = 0;
        prompt = 'In what direction does it act? (x, y, z) ';
        while ~valid
          direction = input(prompt, 's');
          if UtilitiesClass.isValidMember(UtilitiesClass.validDirections, direction)
            valid = 1;
            % translateDirection from value entered (xyz) to ...
            DLoadDir(k,1) = UtilitiesClass.translateFromTo(direction, UtilitiesClass.validDirections, {'BX', 'BY', 'BZ'});
          else
            disp('>>> Invalid <<<');
          end
        end

        valid = 0;
        prompt = 'What is its magnitude? (number) ';
        while ~valid
          magnitude = input(prompt, 's');
          if UtilitiesClass.isNumber(magnitude)
            % convert from string
            DLoadMag(k,1) = str2double(magnitude);
            valid = 1;
          else
            disp('>>> Invalid <<<');
          end
        end

    end

%{
    ... doContinue end
    ... doContinue end
    ... doContinue end
%}

end

if whatNext > 1 % user has opted to write inp file ...

%{
    doCreateInp start ...
    doCreateInp start ...
    doCreateInp start ...
%}

    % % % % PRINTING TO .INP FILE % % % %
    fileName = UtilitiesClass.getFileName();
    fileID=fopen(fileName,'w');
    fprintf(fileID,'*HEADING \n');
    fprintf(fileID,'Developable Double-Corrugated (DDC) Surface Shell Element\n');
    fprintf(fileID,'Generated with Parametric Modelling Tool by Samuel Durley\n');
    fprintf(fileID,'%s\n', fileName);

    % NODE DEFINITON
    fprintf(fileID,'**defining node coordinates \n');
    fprintf(fileID,'*NODE \n');

    for k=1:N
        fprintf(fileID,num2str(k));
        fprintf(fileID,', ');
        for r=1:2
            fprintf(fileID,num2str(T(k,r)));
            fprintf(fileID,', ');
        end
        fprintf(fileID,num2str(T(k,3)));
        fprintf(fileID,'\n');
    end
    
    % ELEMENT DEFINITION
    fprintf(fileID,'**defining element nodes \n');
    fprintf(fileID,'*ELEMENT, TYPE=S4 \n');
    for k=1:NE
        for r=1:4
            fprintf(fileID,num2str(E(k,r)));
            fprintf(fileID,', ');
        end
        fprintf(fileID,num2str(E(k,5)));
        fprintf(fileID,'\n');
    end

    if whatNext > 2 % user opted to enter additional information so add to output

        % ELEMENT SET DEFINITIONS
        fprintf(fileID,'*ELSET, ELSET=ELWHOLESHEET, generate \n');
        fprintf(fileID,'1, %d, 1 \n', NE);

        % NODE SET DEFINITIONS
        fprintf(fileID,'*NSET, NSET=NWHOLESHEET, generate \n');
        fprintf(fileID,'1, %d, 1 \n', N);

        if ~strcmp(boundaries,'') % user has entered boundaries ...

            if UtilitiesClass.isValidMember(UtilitiesClass.validSides, boundaries)
                
                % user selected to fix a single side t, r, b, l ...
                
                % NODE SET SIDE DEFINITIONS
                fprintf(fileID,'*NSET, NSET=TOPSIDE, generate \n');
                fprintf(fileID,'%d, %d, 1 \n', N-(m-1), N);

                fprintf(fileID,'*NSET, NSET=RIGHTSIDE, generate \n');
                fprintf(fileID,'%d, %d, %d \n', m, N, m);

                fprintf(fileID,'*NSET, NSET=BOTTOMSIDE, generate \n');
                fprintf(fileID,'1, %d, 1 \n', m);

                fprintf(fileID,'*NSET, NSET=LEFTSIDE, generate \n');
                fprintf(fileID,'1, %d, %d \n', N-(m-1), m);

            else 
                
                % user selected to fix a set of nodes ...

                % NODE SET DEFINITION
                fprintf(fileID,'*NSET, NSET=FIXEDPOINT \n');
                % output node set entered by user
                fprintf(fileID,'%s \n', boundaries);
            end
        end

        % LOAD SET DEFINITIONS
        for k=1:numCLoad
            if ~strcmp(CLoadNodes(k,1), UtilitiesClass.validCsvAll)
                % not 'all' so create node set
                fprintf(fileID,'*NSET, NSET=CONCLOAD%d \n',k);
                % output node set entered by user
                fprintf(fileID,'%s \n',CLoadNodes(k,1));
            end
        end

        for k=1:numDLoad
            if ~strcmp(DLoadNodes(k,1), UtilitiesClass.validCsvAll)
                % not 'all' so create element set
                fprintf(fileID,'*ELSET, ELSET=DISTLOAD%d \n',k);
                % output element set entered by user
                fprintf(fileID,'%s \n',DLoadNodes(k,1));
            end
        end

        % SHELL AND MATERIAL PROPERTIES
        fprintf(fileID,'*SHELL SECTION, ELSET=ELWHOLESHEET, MATERIAL=MAT1 \n');
        % output user inputs
        fprintf(fileID,'%d, %d \n', thickness, intPoints);
        fprintf(fileID,'*MATERIAL, NAME=MAT1 \n');
        fprintf(fileID,'*ELASTIC \n');
        % output user inputs
        fprintf(fileID,'%f, %f \n', Youngs, Poisson);

        if ~strcmp(boundaries,'')
            % BOUNDARY CONDITIONS
            fprintf(fileID,'*BOUNDARY \n');
            % set default fixedSet
            fixedSet = 'FIXEDPOINT';
            if UtilitiesClass.isValidMember(UtilitiesClass.validSides, boundaries)
                % user selected to fix a single side t, r, b, l ...
                % overwrite fixedSet with the set for the relevant side ...
                if strcmp(boundaries, 't')
                    fixedSet = 'TOPSIDE';
                end
                if strcmp(boundaries, 'r')
                    fixedSet = 'RIGHTSIDE';
                end
                if strcmp(boundaries, 'b')
                    fixedSet = 'BOTTOMSIDE';
                end
                if strcmp(boundaries, 'l')
                    fixedSet = 'LEFTSIDE';
                end
            end
            % output the correct fixedSet - side or user defined set
            fprintf(fileID,'%s, 1, 6 \n', fixedSet);
        end

        % STEP DEFINITION
        fprintf(fileID,'*STEP, NAME=STEP-1,NLGEOM=NO \n');
        fprintf(fileID,'*STATIC \n');

        if numCLoad > 0
            % concentrated loads
            fprintf(fileID,'*CLOAD \n');
            for k=1:numCLoad
                if strcmp(CLoadNodes(k,1), UtilitiesClass.validCsvAll)
                    % user entered 'all' so use node whole sheet
                    fprintf(fileID,'NWHOLESHEET, %s, %d \n', CLoadDir(k,1), CLoadMag(k,1));
                else
                    fprintf(fileID,'CONCLOAD%d, %s, %d \n', k, CLoadDir(k,1), CLoadMag(k,1));
                end
            end
        end

        if numDLoad > 0
            % distributed loads
            fprintf(fileID,'*DLOAD \n');
            for k=1:numDLoad
                if strcmp(DLoadNodes(k,1), UtilitiesClass.validCsvAll)
                    % user entered 'all' so use element whole sheet
                    fprintf(fileID,'ELWHOLESHEET, %s, %d \n', DLoadDir(k,1), DLoadMag(k,1));
                else
                    fprintf(fileID,'DISTLOAD%d, %s, %d \n', k, DLoadDir(k,1), DLoadMag(k,1));
                end
            end
        end

    end % whatNext > 2

    fprintf(fileID,'*END STEP \n');

    % CLOSE TEXT FILE
    fclose(fileID);

    disp(['.inp file written - ', fileName]);

%{
    ... doCreateInp end
    ... doCreateInp end
    ... doCreateInp end
%}

end

%{
doEnd start ...
doEnd start ...
doEnd start ...
%}

disp('ending');

%{
... doEnd end
... doEnd end
... doEnd end
%}
   
   



